import { routes } from './Routes'; 
import axios from 'axios';
import { credentials } from '../Variables';


class SpotifyApi {
  constructor() {
    this.spotifyApi = axios.create({
      baseURL:routes.base_url,
    });
  }
  
  getAlbums(accessToken, country){
    let config = { 
      headers: { Authorization: `Bearer ${accessToken}`}
      }
    return this.spotifyApi.get(`${routes.categories}?country=${country}`, config )
    .then(({ data }) => data);
  }
  
  getPlaylists(accessToken, categoryId){
    let config = { 
      headers: { Authorization: `Bearer ${accessToken}`}
      }
    return this.spotifyApi.get(`${routes.categories}/${categoryId}/playlists?limit=10`, config )
    .then(({ data }) => data);
  }

  getToken(callback){
    const hash = window.location.hash
      .substring(1)
      .split('&')
      .reduce((initial, item) => {
        if (item) {
          let parts = item.split('=');
          initial[parts[0]] = decodeURIComponent(parts[1]);
        }
        return initial;
      }, {});
    window.location.hash = '';

   
    let { access_token } = hash;
    const redirect = scopedRedirect();
        callback(access_token ? access_token : () => { redirect() } )
    return hash
  }
}
const scopedRedirect = () => {
  return () => window.location = `${credentials.authEndpoint}?client_id=${credentials.clientId}&redirect_uri=${credentials.redirectUri}&response_type=token`;
}

const spotifyApi = new SpotifyApi();

export default spotifyApi;