export const credentials = {
     //this should be on .env, only added here for demostration purpose
     clientId : 'e359ff99bd5641089909a0175d2131be',
     redirectUri : 'http://localhost:3000',
     authEndpoint : 'https://accounts.spotify.com/authorize',
}

export const lang = {
     es:'ES'
}