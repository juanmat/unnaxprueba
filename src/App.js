import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import SelectorContainer from './components/SelectorContainer';

export default function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SelectorContainer />} />
        <Route />
      </Routes>
    </BrowserRouter>
  );
}

