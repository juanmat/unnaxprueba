import React, { useState, useEffect } from 'react';
import spotifyApi from "../../remote/SpotifyApi";
import CategorySelect from '../CategorySelect';
import { lang } from '../../Variables'

export default function Categories(props) {
  const [categorySelected, setcategorySelected] = useState({
    items: [],
    loading: true,
    error: false,
    errorMessage: ""
  })

  useEffect(() => {
    spotifyApi.getAlbums(props.accessToken, lang.es)
      .then(({ categories: { items } }) => {
        setcategorySelected({ items, loading: false })
      })
      .catch(error => setcategorySelected({ error: true, errorMessage: error.response.error }));
  }, [])
  return (
    <section className="categories">
      <CategorySelect categories={categorySelected.items} selectedId={props.categoryId} setCategoryId={props.setCategoryId} />
    </section>
  );
}


