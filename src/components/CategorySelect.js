import React, { useState, useEffect } from 'react';

export default function CategorySelect(props) {
  const [selected, setselected] = useState('')
  const [categories, setCategories] = useState(props.categories)

  useEffect(() => {
    setCategories(props.categories)
    updateSelected(props.selectedId)
  }, [props])

  const updateSelected = (selectedId) => {
    if (selectedId !== selected) {
      setselected(selectedId)
    }
  }

  const updateSelection = (selected) => {
    props.setCategoryId(selected)
  }

  const handleOnChange = (e) => {
    const { value: selected } = e.target;
    updateSelection(selected)
  }

  const renderItem = (item) => {
    return <option key={item.id} value={item.id}>{item.name}</option>
  }

  return (
    <div className="head">
      <img className="spotify-logo" src="https://developer.spotify.com/assets/branding-guidelines/logo.png" />
      <select className="category-dropdown" value={selected} onChange={handleOnChange}>
        <option key={"none"} value="" disabled>Elije una categoria</option>
        {categories && categories?.map(item => renderItem(item))}
      </select>
    </div>
  )
}

