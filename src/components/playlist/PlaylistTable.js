import React from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

export default function PlaylistTable(props) {
  const { playlists } = props;
  return (
    <Table>
      <TableBody>
        {playlists && playlists.map((playlist, index) => (
          <TableRow key={playlist.name}>
            <TableCell align="center" className={`imageList`}><img src={`${playlist.images[0].url}`} />
            </TableCell>
            <TableCell >
              <a href={`${playlist.external_urls.spotify}`}>{playlist.name}</a>
            </TableCell>
            <TableCell component="th" scope="playlist">
              {++index}
            </TableCell>
          </TableRow>
        ))}    
      </TableBody>
    </Table>
  );
}

