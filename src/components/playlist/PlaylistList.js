import React, { useState, useEffect, useCallback } from 'react';
import spotifyApi from "../../remote/SpotifyApi";
import PlaylistTable from './PlaylistTable';

export default function PlaylistList(props) {
  const [playlists, setPlaylists] = useState([]);
  const { accessToken, category } = props;
  const [appearing, setAppearing] = useState(true)

  let appear = () => {
    setTimeout(() => {
      setAppearing(false)
    }, 1500);
  }
  appear()

  /*const fetchPlaylists = useCallback(() => {
    setAppearing(true)
    spotifyApi.getPlaylists(accessToken, category)
      .then(data => {
        const { items } = data.playlists;

        setPlaylists(items);
        appear();
      })
  }, [accessToken, category])

  useEffect(() => {
    fetchPlaylists();
  }, [fetchPlaylists]);*/

  useEffect(() => {
    setAppearing(true)
    spotifyApi.getPlaylists(accessToken, category)
      .then(data => {
        const { items } = data.playlists;

        setPlaylists(items);
        appear();
      })
  }, [accessToken, category])


  return (
    <article className={`table-container ${!appearing && "appear"}`}>
      {playlists.length > 0 && <PlaylistTable playlists={playlists} />}
    </article>
  );
}