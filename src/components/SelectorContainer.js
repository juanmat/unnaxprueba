import React, { useEffect, useState } from 'react'
import spotifyApi from '../remote/SpotifyApi';
import Categories from '../components/categories/Categories';
import Playlists from '../components/playlist/PlaylistList';

const SelectorContainer = () => {
    const [selection, setSelectionState] = useState({
        categoryId: '',
        showPlaylists: false,
    });
    const [access_token, setAccess_token] = useState('');

    useEffect(() => {
        spotifyApi.getToken(setAccess_token)
    }, [])

    const setCategoryId = (categoryId) => {
        setSelectionState({ categoryId, showPlaylists: true });
    }

    const Showselector = () => {
        return access_token ?
            <Categories accessToken={access_token} categoryId={selection.categoryId} setCategoryId={setCategoryId} /> :
            <h2>Please Refresh to Log In</h2>;
    };

    const Showplaylist = () => {
        return selection.showPlaylists && <Playlists accessToken={access_token} category={selection.categoryId} />;
    }


    return (
        <>
            <Showselector />
            <Showplaylist />
        </>
    )
}

export default SelectorContainer;